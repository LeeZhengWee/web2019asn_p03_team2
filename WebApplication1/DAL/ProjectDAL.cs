﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.Models;

namespace WebApplication1.DAL
{
    public class ProjectDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public ProjectDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            //Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "Student_EportfolioConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        public int Add(Project project)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO Project (Title, Description) " +
            "OUTPUT INSERTED.ProjectId " +
            "VALUES(@Title, @Description)", conn);
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@Title", project.Title);
            cmd.Parameters.AddWithValue("@Description", project.Description);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            project.ProjectId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return project.ProjectId;
        }
        public int Addpm(ProjectMember projectMember)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO ProjectMember (ProjectID,StudentID, Role) " +
            "VALUES(@ProjectID,@StudentID, @Role)", conn);
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@ProjectID",projectMember.ProjectID);
            cmd.Parameters.AddWithValue("@StudentID", projectMember.StudentID);
            cmd.Parameters.AddWithValue("@Role", "Leader");
            
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            //A connection to database must be opened before any operations made.
            conn.Open();
            da.Fill(result);
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return projectMember.ProjectID;
        }
        public int Addmember(ProjectViewModel projectMember)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO ProjectMember (ProjectID,StudentID, Role) " +
            "VALUES(@ProjectID,@StudentID, @Role)", conn);
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@ProjectID", projectMember.ProjectId);
            cmd.Parameters.AddWithValue("@StudentID", projectMember.StudentID);
            cmd.Parameters.AddWithValue("@Role", "Member");

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            //A connection to database must be opened before any operations made.
            conn.Open();
            da.Fill(result);
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement

            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return projectMember.ProjectId;
        }
        public List<Project> GetAllProjects()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand(
            "SELECT * FROM Project ORDER BY ProjectID", conn);
            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "ProjectDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<Project> projectList = new List<Project>();
            foreach (DataRow row in result.Tables["ProjectDetails"].Rows)
            {
                projectList.Add(
                new Project
                {
                    ProjectId = Convert.ToInt32(row["ProjectID"]),
                    Title = Convert.ToString(row["Title"]),
                    Description = Convert.ToString(row["Description"]),
                    ProjectPoster = Convert.ToString(row["ProjectPoster"]),
                    ProjectURL = Convert.ToString(row["ProjectURL"])
                }
                );
            }
            return projectList;
        }
        public List<ProjectMember> GetAllProjectMembers()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand(
            "SELECT * FROM ProjectMember ORDER BY ProjectID", conn);
            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "ProjectMemberDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<ProjectMember> projectMemberList = new List<ProjectMember>();
            foreach (DataRow row in result.Tables["ProjectMemberDetails"].Rows)
            {
                projectMemberList.Add(
                new ProjectMember
                {
                    ProjectID = Convert.ToInt32(row["ProjectID"]),
                    StudentID = Convert.ToInt32(row["StudentID"]),
                    Role = row["Role"].ToString()
                }
                );
            }
            return projectMemberList;
        }
        // Return number of row updated
        public int Update(ProjectViewModel project)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("UPDATE Project SET Title=@Title, Description=@Description, ProjectURL=@ProjectURL" +
            " WHERE ProjectID = @selectedProjectID", conn);
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from the respective property of “staff” object.
            cmd.Parameters.AddWithValue("@Description", project.Description);
            cmd.Parameters.AddWithValue("@Title", project.Title);
            cmd.Parameters.AddWithValue("@ProjectURL", project.ProjectURL);
            cmd.Parameters.AddWithValue("@selectedProjectID", project.ProjectId  );
            //Open a database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection.
            conn.Close ();
        return count;
        }
        public string changephoto(Project project)
        {
            SqlCommand cmd = new SqlCommand
                ("UPDATE Project SET ProjectPoster = @ProjectPoster " +
                "WHERE ProjectID = @selectedProjectID", conn);

            cmd.Parameters.AddWithValue("@selectedProjectId", project.ProjectId);
            cmd.Parameters.AddWithValue("@ProjectPoster", project.ProjectPoster);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            //A connection to database must be opened before any operations made.
            conn.Open();
            da.Fill(result);
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement

            //A connection should be closed after operations.
            conn.Close();
            return project.ProjectPoster;

        }
        public Project GetDetails(int projectId)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand
            ("SELECT * FROM Project WHERE ProjectId = @selectedProjectId", conn);
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedProjectId", projectId);
            //Instantiate a DataAdapter object, pass the SqlCommand
            //object “cmd” as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object “result"
            DataSet result = new DataSet();
            //Open a database connection.
            conn.Open();
            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            da.Fill(result, "StaffDetails");
            //Close the database connection
            conn.Close();
            Project project = new Project();
            if (result.Tables["StaffDetails"].Rows.Count > 0)
            {
                project.ProjectId = projectId;
                // Fill staff object with values from the DataSet
                DataTable table = result.Tables["StaffDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectId"]))
                    project.ProjectId = Convert.ToInt32(table.Rows[0]["ProjectId"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Title"]))
                    project.Title = table.Rows[0]["Title"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    project.Description = Convert.ToString(table.Rows[0]["Description"]);
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectPoster"]))
                    project.ProjectPoster = Convert.ToString(table.Rows[0]["ProjectPoster"]);
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectURL"]))
                    project.ProjectURL = table.Rows[0]["ProjectURL"].ToString();

                return project;
            }
            else
            {
                return null;
            }
        }
        public ProjectViewModel GotDetails(int projectId)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand
            ("SELECT * FROM Project WHERE ProjectId = @selectedProjectId", conn);
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedProjectId", projectId);
            //Instantiate a DataAdapter object, pass the SqlCommand
            //object “cmd” as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object “result"
            DataSet result = new DataSet();
            //Open a database connection.
            conn.Open();
            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            da.Fill(result, "StaffDetails");
            //Close the database connection
            conn.Close();
            ProjectViewModel project = new ProjectViewModel();
            if (result.Tables["StaffDetails"].Rows.Count > 0)
            {
                project.ProjectId = projectId;
                // Fill staff object with values from the DataSet
                DataTable table = result.Tables["StaffDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectId"]))
                    project.ProjectId = Convert.ToInt32(table.Rows[0]["ProjectId"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Title"]))
                    project.Title = table.Rows[0]["Title"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    project.Description = Convert.ToString(table.Rows[0]["Description"]);
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectPoster"]))
                    project.ProjectPoster = Convert.ToString(table.Rows[0]["ProjectPoster"]);
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectURL"]))
                    project.ProjectURL = table.Rows[0]["ProjectURL"].ToString();

                return project;
            }
            else
            {
                return null;
            }
        }
    }
}
