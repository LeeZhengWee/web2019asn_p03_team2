﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication1.DAL
{
    public class StudentDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public StudentDAL()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            //connection strings
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("Student_EportfolioConnectionString");
            conn = new SqlConnection(strConn);
        }

        //Verify student Student login  
        public Student VerifyStudentLogin(string username, string password)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE EmailAddr=@username AND Password=@password", conn);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            Student student = new Student();

            conn.Open();
            da.Fill(result, "StudentDetails");
            conn.Close();

            DataRowCollection rows = result.Tables["StudentDetails"].Rows;
            if (rows.Count == 1)
            {


                student.StudentID = Convert.ToInt32(rows[0]["StudentID"]);
                student.Name = rows[0]["Name"].ToString();



                return student;
            }

            return student;
        }

        //verify email exist when creating student
        public bool IsEmailExist(string email)
        {
            SqlCommand cmd = new SqlCommand
            ("SELECT StudentID FROM Student WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet.
            daEmail.Fill(result, "EmailDetails");
            conn.Close();
            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //The email exists for another student
            else
                return false; // The email address given does not exist
        }


        //create student conn
        public int Add(Student student)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated studentID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO Student (Name, EmailAddr , Password, MentorID, Course) " +
            "OUTPUT INSERTED.StudentID " +
            "VALUES(@name, @emailaddr, @password, @mentorId, '' )", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@name", student.Name);
            cmd.Parameters.AddWithValue("@emailaddr", student.EmailAddr);
            cmd.Parameters.AddWithValue("@password", "p@55Student");
            cmd.Parameters.AddWithValue("@mentorId", student.MentorID);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StudentID after executing the INSERT SQL statement
            student.StudentID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return student.StudentID;
        }

        //Student table conn 
        public Student GetDetails(int studentId)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Student WHERE StudentID = @selectedStudentID", conn);

            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "StudentDetails");

            conn.Close();

            Student student = new Student();


            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                student.StudentID = studentId;
                DataTable table = result.Tables["StudentDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    student.Name = table.Rows[0]["Name"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                    student.Photo = table.Rows[0]["Photo"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Course"]))
                    student.Course = table.Rows[0]["Course"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    student.Description = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                    student.Achievement = table.Rows[0]["Achievement"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                    student.EmailAddr = table.Rows[0]["EmailAddr"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                    student.ExternalLink = table.Rows[0]["ExternalLink"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Password"]))
                    student.Password = table.Rows[0]["Password"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["MentorID"]))
                    student.MentorID = Convert.ToInt32(table.Rows[0]["MentorID"]);
                return student;

            }
            else
            {
                return null;
            }
        }

     

        //Create update conn
        public StudentUpdate GetStudentUpdate(int studentId)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Student WHERE StudentID = @selectedStudentID", conn);

            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);


            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "StudentDetails");

            conn.Close();

            StudentUpdate studentupdate = new StudentUpdate();

            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                studentupdate.StudentID = studentId;

                DataTable table = result.Tables["StudentDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    studentupdate.Description = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                    studentupdate.Achievement = table.Rows[0]["Achievement"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                    studentupdate.Photo = Convert.ToString(table.Rows[0]["Photo"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    studentupdate.Name = table.Rows[0]["Name"].ToString();

                return studentupdate;
            }
            else
            {
                return null;
            }
        }

        //Student Update method
        public int Update(StudentUpdate student)
        {
            SqlCommand cmd = new SqlCommand
                ("UPDATE Student SET Description=@description, Achievement=@achievement" + " WHERE StudentID = @selectedStudentID", conn);

            SqlCommand cmd2 = new SqlCommand
                ("INSERT INTO StudentSkillSet (StudentID, SkillSetID)" +
            "VALUES(@studentId, @skillSetId)", conn);

            //Define the parameters used in SQL statement, value for each paremeter
            //is retrieved from the respective property of "student" object.
            cmd.Parameters.AddWithValue("@description", student.Description);
            cmd.Parameters.AddWithValue("@achievement", student.Achievement);


            cmd2.Parameters.AddWithValue("@studentId", student.StudentID);
            cmd2.Parameters.AddWithValue("@skillSetId", student.SkillSetID);




            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataSet result = new DataSet();

            cmd.Parameters.AddWithValue("@selectedStudentID", student.StudentID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();
            da.Fill(result);

            conn.Close();

            return count;
        }

        public int Update2(StudentUpdate student)
        {
            SqlCommand cmd = new SqlCommand
               ("UPDATE Student SET Description=@description, Achievement=@achievement" + " WHERE StudentID = @selectedStudentID", conn);

            //Define the parameters used in SQL statement, value for each paremeter
            //is retrieved from the respective property of "student" object.
            cmd.Parameters.AddWithValue("@description", student.Description);
            cmd.Parameters.AddWithValue("@achievement", student.Achievement);


            cmd.Parameters.AddWithValue("@selectedStudentID", student.StudentID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

             return count;
        }


        //create student view conn for photo 
        public StudentUpdate GetStudentView(int studentId)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Student WHERE StudentID = @selectedStudentID", conn);

            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);


            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "UploadDetails");

            conn.Close();

            StudentUpdate studentupload = new StudentUpdate();

            if (result.Tables["UploadDetails"].Rows.Count > 0)
            {
                studentupload.StudentID = studentId;

                DataTable table = result.Tables["UploadDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    studentupload.Name = table.Rows[0]["Name"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                    studentupload.Photo = table.Rows[0]["Photo"].ToString();
    

                return studentupload;
            }
            else
            {
                return null;
            }
        }
       
        //Method for photo upload 
        public void Upload(string fileName, int studentid)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Photo=@photo WHERE StudentID=@selectedStudentId", conn);

            cmd.Parameters.AddWithValue("@photo", fileName);
            cmd.Parameters.AddWithValue("selectedStudentId", studentid);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }


        //create studentskillset conn
        public List<StudentSkillSet> GetStudentSkills(int studentId)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM StudentSkillSet WHERE StudentID = @selectedStudentID", conn);

            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);


            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "StudentSkillDetails");

            conn.Close();

            List<StudentSkillSet> StudentSkillSets = new List<StudentSkillSet>();
            foreach (DataRow row in result.Tables["StudentSkillDetails"].Rows)
            {
                StudentSkillSets.Add(new StudentSkillSet
                {
                    SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                    StudentID = Convert.ToInt32(row["StudentID"])
                });
            }
            return StudentSkillSets;


        }



        //Create Mentor list 
        public List<Lecturer> GetMentor(int lecturerid)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Lecturer WHERE LecturerID = @lecturerid", conn);

            cmd.Parameters.AddWithValue("@lecturerid", lecturerid);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "LecturerDetails");
            conn.Close();

            List<Lecturer> lecturerList = new List<Lecturer>();
            foreach (DataRow row in result.Tables["LecturerDetails"].Rows)
            {
                lecturerList.Add(
                    new Lecturer
                    {
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                        Name = row["Name"].ToString(),
                        EmailAddr = row["EmailAddr"].ToString(),
                        Password = row["Password"].ToString(),
                        Description = row["Description"].ToString()
                    }
                    );
            }

            return lecturerList;
        }

        //Create suggestion list 
        public List<Suggestion> GetSuggestions(int id)
        {
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Suggestion WHERE StudentID = @selectedID", conn
            );

            cmd.Parameters.AddWithValue("@selectedID", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            da.Fill(result, "SuggestionsResult");
            conn.Close();

            List<Suggestion> suggestionList = new List<Suggestion>();

            foreach (DataRow row in result.Tables["SuggestionsResult"].Rows)
            {
                suggestionList.Add(
                new Suggestion
                {
                    SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                    LecturerID = Convert.ToInt32(row["LecturerID"]),
                    StudentID = Convert.ToInt32(row["StudentID"]),
                    Description = row["Description"].ToString(),
                    Status = Convert.ToChar(row["Status"]),
                    DateCreated = Convert.ToDateTime(row["DateCreated"])
                }
                );
            }

            foreach (Suggestion s in suggestionList)
            {
                s.LecturerName = GetLecturerName(Convert.ToInt32(s.LecturerID));
            }

            return suggestionList;
        }

        //Geting lecturer name only 
        public string GetLecturerName(int lecturerid)
        {
            string name = "";
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Lecturer WHERE LecturerID = @lecturerid", conn);
            cmd.Parameters.AddWithValue("@lecturerid", lecturerid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "NameResult");
            conn.Close();
            foreach (DataRow row in result.Tables["NameResult"].Rows)
            {
                name = row["Name"].ToString();
            }

            return name;
        }

        //method for Acknowledge post 
        public int Acknowledge(int suggestionid)
        {
            SqlCommand cmd = new SqlCommand
                ("UPDATE Suggestion SET Status=@status" + " WHERE SuggestionID = @selectedSuggestionID", conn);

            //Define the parameters used in SQL statement, value for each paremeter
            //is retrieved from the respective property of "student" object.
            cmd.Parameters.AddWithValue("@status", "Y");
        

 
            DataSet result = new DataSet();


            cmd.Parameters.AddWithValue("@selectedSuggestionID", suggestionid);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        //Getting Mentor ID 
        public List<Lecturer> GetAllMentorID()
        {
            //Instantiate a SqlCommand object, supply it with a              
            //SELECT SQL statement that operates against the database,              
            //and the connection object for connecting to the database.             
            SqlCommand cmd = new SqlCommand("SELECT * FROM Lecturer ORDER BY LecturerID", conn);

            //Instantiate a DataAdapter object and pass the              
            //SqlCommand object created as parameter.            
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain records get from database             
            DataSet result = new DataSet();

            //Open a database connection             
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its SqlCommand object to fetch data to a table "StaffDetails" in DataSet "result".             
            da.Fill(result, "MentorDetails");
            //Close the database connection                        
            conn.Close();

            //Transferring rows of data in DataSet’s table to “SkillSet” objects             
            List<Lecturer> mentorList = new List<Lecturer>();
            foreach (DataRow row in result.Tables["MentorDetails"].Rows)
            {
                mentorList.Add(new Lecturer
                {
                    LecturerID = Convert.ToInt32(row["LecturerID"]),
              
                });
            }
            return mentorList;
        }



    }
}
