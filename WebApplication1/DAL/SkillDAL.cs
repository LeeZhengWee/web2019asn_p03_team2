﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.Models;

namespace WebApplication1.DAL
{
    public class SkillDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;


        //Constructor         
        public SkillDAL()
        {
            //Locate the appsettings.json file            
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettings.json file            
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("Student_EportfolioConnectionString");

            //Instantiate a SqlConnection object with the Connection String read.              
            conn = new SqlConnection(strConn);
        }



        //Get All SkillSet 
        public List<SkillSets> GetAllSkill()
        {
            //Instantiate a SqlCommand object, supply it with a              
            //SELECT SQL statement that operates against the database,              
            //and the connection object for connecting to the database.             
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet ORDER BY SkillSetID", conn);

            //Instantiate a DataAdapter object and pass the              
            //SqlCommand object created as parameter.            
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain records get from database             
            DataSet result = new DataSet();

            //Open a database connection             
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its SqlCommand object to fetch data to a table "StaffDetails" in DataSet "result".             
            da.Fill(result, "SkillDetails");
            //Close the database connection                        
            conn.Close();

            //Transferring rows of data in DataSet’s table to “SkillSet” objects             
            List<SkillSets> skillList = new List<SkillSets>();
            foreach (DataRow row in result.Tables["SkillDetails"].Rows)
            {
                skillList.Add(new SkillSets
                {
                    SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                    SkillSetName = row["SkillSetName"].ToString()
                });
            }
            return skillList;
        }

        public int Add(SkillSets skillSets)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement which will return the auto-generated StaffID after insertion,    
            //and the connection object for connecting to the database.     
            SqlCommand cmd = new SqlCommand("INSERT INTO SkillSet (SkillSetName) OUTPUT INSERTED.SkillSetID VALUES(@skillSetName)", conn);

            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from respective class's property.     
            cmd.Parameters.AddWithValue("@skillSetName", skillSets.SkillSetName);

            //A connection to database must be opened before any operations made.  
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated  
            //StaffID after executing the INSERT SQL statement   
            skillSets.SkillSetID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.    
            conn.Close();
            //Return id when no error occurs.     
            return skillSets.SkillSetID;
        }

        public int Delete(int skillSetID)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement    
            //to delete a staff record specified by a Staff ID.     

            //cannot be null though
            SqlCommand cmd1 = new SqlCommand("DELETE FROM StudentSkillSet WHERE StudentSkillSet.SkillSetID = @selectSkillSetID", conn);
            cmd1.Parameters.AddWithValue("@selectSkillSetID", skillSetID);
            SqlCommand cmd2 = new SqlCommand("DELETE FROM SkillSet WHERE SkillSetID = @selectSkillSetID", conn);
            cmd2.Parameters.AddWithValue("@selectSkillSetID", skillSetID);

            //Open a database connection.    
            conn.Open();
            int rowCount;
            //Execute the DELETE SQL to remove the staff record.  
            rowCount = cmd1.ExecuteNonQuery() + cmd2.ExecuteNonQuery();
            //Close database connection.   
            conn.Close();
            //Return number of row of staff record deleted.     
            return rowCount;
        }

        public SkillSets GetDetails(int skillSetID)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL statement which retrieves all attributes of a staff record.     
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet WHERE SkillSetID = @selectedSkillSetID", conn);

            //Define the parameter used in SQL statement, value for the parameter is retrieved from the method parameter “staffId”. 
            cmd.Parameters.AddWithValue("@selectedSkillSetID", skillSetID);

            //Instantiate a DataAdapter object, pass the SqlCommand object “cmd” as parameter.     
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object “result"     
            DataSet result = new DataSet();

            //Open a database connection.    
            conn.Open();

            //Use DataAdapter to fetch data to a table "SkillDetails" in DataSet.     
            da.Fill(result, "SkillDetails");

            //Close the database connection     
            conn.Close();

            SkillSets skillSets = new SkillSets();

            if (result.Tables["SkillDetails"].Rows.Count > 0)
            {
                skillSets.SkillSetID = skillSetID;
                // Fill staff object with values from the DataSet         
                DataTable table = result.Tables["SkillDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetName"]))
                    skillSets.SkillSetName = table.Rows[0]["SkillSetName"].ToString();
                return skillSets;
                // No error occurs
            }
            else
            {
                return null; // Record not found     
            }

        }


        public int Update(SkillSets skillsets)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE and the connection object for connecting to the database.     
            SqlCommand cmd = new SqlCommand("UPDATE SkillSet SET SkillSetName=@skillSetName WHERE SkillSetID = @selectedSkillSetID", conn);

            //Define the parameters used in SQL statement, value for each parameter is retrieved from the respective property of “staff” object.     
            cmd.Parameters.AddWithValue("@skillSetName", skillsets.SkillSetName);

            cmd.Parameters.AddWithValue("@selectedSkillSetID", skillsets.SkillSetID);
            //if (staff.BranchNo != null)
            //    // A branch is assigned         
            //    cmd.Parameters.AddWithValue("@branchNo", staff.BranchNo.Value);

            //else
            //    // No branch is assigned         
            //    cmd.Parameters.AddWithValue("@branchNo", DBNull.Value);

            //Open a database connection.     
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE     
            int count = cmd.ExecuteNonQuery();
            //Close the database connection.    
            conn.Close();
            return count;
        }

      

        public List<Student> GetAllStudentSkillSet(int SkillSetID)
        {
            SqlCommand cmd = new SqlCommand("SELECT S.* FROM Student S INNER JOIN StudentSkillSet SSS ON SSS.StudentID = S.StudentID WHERE SSS.SkillSetID = @id", conn);
            cmd.Parameters.AddWithValue("@id", SkillSetID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            da.Fill(result, "Students");
            conn.Close();

            if (result.Tables["Students"].Rows.Count == 0)
                return null;

            List<Student> students = new List<Student>();
            foreach (DataRow row in result.Tables["Students"].Rows)
            {
                students.Add(new Student
                {
                    StudentID = Convert.ToInt32(row["StudentID"]),
                    Name = row["Name"].ToString(),
                    Photo = row["Photo"].ToString(),
                });
            }

            return students;
        }

        public bool IsSkillNameExist(string skillname)
        {
            SqlCommand cmd = new SqlCommand("SELECT SkillSetID FROM SkillSet WHERE SkillSetName = @selectedSkill", conn);
            cmd.Parameters.AddWithValue("@selectedSkill", skillname);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet(); conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet.   
            da.Fill(result, "Details");
            conn.Close();
            if (result.Tables["Details"].Rows.Count > 0)
                return true; //The email exists for another staff   
            else
                return false;
            // The email address given does not exist }

        }
    }
}
