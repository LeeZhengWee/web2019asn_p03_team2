﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.DAL
{
    public class LecturerDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;


        public LecturerDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            //Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "Student_EportfolioConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Student> GetAllStudents()
        {
            List<Student> studentList = new List<Student>();

            SqlCommand cmd = new SqlCommand("SELECT * FROM Student ORDER BY StudentID", conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();


            conn.Open();


            da.Fill(result, "StudentDetails");

            conn.Close();

            foreach (DataRow row in result.Tables["StudentDetails"].Rows)
            {
                studentList.Add(
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        Name = Convert.ToString(row["Name"]),
                        Course = Convert.ToString(row["Course"]),
                        Description = Convert.ToString(row["Description"]),
                        Achievement = Convert.ToString(row["Achievement"]),
                        ExternalLink = Convert.ToString(row["ExternalLink"]),
                        EmailAddr = Convert.ToString(row["EmailAddr"]),
                        Password = Convert.ToString(row["Password"]),
                        MentorID = Convert.ToInt32(row["MentorID"])
                    });
            }

            return studentList;
        }

        public Lecturer VerifyLecturerLogin(string username, string password)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Lecturer WHERE EmailAddr=@username AND Password=@password", conn);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "LecturerDetails");
            conn.Close();

            DataRowCollection rows = result.Tables["LecturerDetails"].Rows;
            if (rows.Count == 1)
            {
                Lecturer lecturer = new Lecturer
                {
                    LecturerID = Convert.ToInt32(rows[0]["LecturerID"]),
                    Name = rows[0]["Name"].ToString()

                };

                return lecturer;
            }

            return null;
        }

        public int Add (Suggestion suggestion)
        {

            SqlCommand cmd = new SqlCommand ("INSERT INTO Suggestion(LecturerID, StudentID, Description) " + "OUTPUT INSERTED.SuggestionID " +
                "VALUES(@lecturer, @student,  @description)", conn);

            cmd.Parameters.AddWithValue("@lecturer", suggestion.LecturerID);
            cmd.Parameters.AddWithValue("@student", suggestion.StudentID);
            cmd.Parameters.AddWithValue("@description", suggestion.Description);

            conn.Open();

            suggestion.SuggestionID = (int)cmd.ExecuteScalar();

            conn.Close();

            return suggestion.SuggestionID;
        }

        public List<Suggestion> GetSuggestions()
        {
            List<Suggestion> suggestionList = new List<Suggestion>();

            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion ORDER BY SuggestionID", conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();


            conn.Open();


            da.Fill(result, "Suggestions");

            conn.Close();

            foreach (DataRow row in result.Tables["Suggestions"].Rows)
            {
                suggestionList.Add(
                    new Suggestion
                    {
                        SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        Description = Convert.ToString(row["Description"])
                    });
            }

            return suggestionList;
        }

        public string AddLecturer(Lecturer lecturer)
        {

            SqlCommand cmd = new SqlCommand("INSERT INTO Lecturer(Name, EmailAddr, Password, Description) " + "OUTPUT INSERTED.Name " +
                "VALUES(@name, @emailaddr, @password, @description)", conn);

            cmd.Parameters.AddWithValue("@name", lecturer.Name);
            cmd.Parameters.AddWithValue("@emailaddr", lecturer.EmailAddr);
            cmd.Parameters.AddWithValue("@password", "p@55Mentor");
            cmd.Parameters.AddWithValue("@description", lecturer.Description);

            conn.Open();

            lecturer.Name = (string)cmd.ExecuteScalar();

            conn.Close();

            return lecturer.Name;
        }

        public void UpdatePassword(string lecturerID, string password)
        {
            SqlCommand cmd = new SqlCommand
                ("UPDATE Lecturer SET Password = @password WHERE LecturerID = @selectedLecturerID", conn);

            //Define the parameters used in SQL statement, value for each paremeter
            //is retrieved from the respective property of "student" object.
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@selectedLecturerID", lecturerID);

            conn.Open();

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public bool accountChecker(string lecturer, string password)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT LecturerID, Name FROM Lecturer WHERE LecturerID = @selectedLecturerID AND Password = @selectedPass", conn);
            cmd.Parameters.AddWithValue("@selectedLecturerID", lecturer);
            cmd.Parameters.AddWithValue("@selectedPass", password);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            da.Fill(result, "AccountDetails");
            conn.Close();

            if (result.Tables["AccountDetails"].Rows.Count == 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

    }
}
