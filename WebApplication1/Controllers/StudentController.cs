﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication1.DAL;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    public class StudentController : Controller

    {

        private StudentDAL studentContext = new StudentDAL();
        private SkillDAL skillContext = new SkillDAL();


        // GET: Student

        public ActionResult Index()
        {

            return View();

        }

        public ActionResult UpdateSuccess()
        {
            return View();
        }

     
        // GET: Student/Create
        public ActionResult StudentCreate()
        {
            ViewData["MentorList"] = GetAllMentorID();
            Student student = new Student { Password = "p@55Student" };
            return View(student);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StudentCreate(Student student)
        {

            ViewData["MentorList"] = GetAllMentorID();
            if (ModelState.IsValid)
            {
                student.StudentID = studentContext.Add(student);

                return RedirectToAction("StudentLogin", "Home");
            }

            else
                return View(student);
        }

        // GET: Student/Update/

        [HttpGet]
        public ActionResult Update()
        {


            StudentUpdate student = studentContext.GetStudentUpdate(Convert.ToInt32(HttpContext.Session.GetString("ID")));


            ViewData["SkillList"] = AvailSkillSet();


            if (student == null)
            {
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // POST: Student/Update/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(StudentUpdate student)
        {
            System.Diagnostics.Debug.WriteLine(ModelState.IsValid);
            ViewData["SkillList"] = AvailSkillSet();


            if (ModelState.IsValid)
            {

                studentContext.Update(student);


                return RedirectToAction("UpdateSuccess", "Student");
            }
            //Input validation fails, return to the view    
            //to display error message    
            return View(student);
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Student/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Delete(int id, IFormCollection collection)
        {

            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //Get all skills
        private List<SelectListItem> GetAllSkills()

        {
            List<SelectListItem> skills = new List<SelectListItem>();
            List<SkillSets> skillList = skillContext.GetAllSkill();

            foreach (SkillSets skill in skillList)
            {
                skills.Add(
                    new SelectListItem
                    {
                        Value = skill.SkillSetID.ToString(),
                        Text = skill.SkillSetName
                    });
            }
            return skills;


        }

        //Get all Mentor ID
        private List<SelectListItem> GetAllMentorID()

        {
            List<SelectListItem> Mentors = new List<SelectListItem>();
            List<Lecturer> mentorList = studentContext.GetAllMentorID();


            foreach (Lecturer m in mentorList)
            {
                Mentors.Add(
                    new SelectListItem
                    {
                        Value = m.LecturerID.ToString(),
                        Text = m.LecturerID.ToString()
                    });
            }
            return Mentors;


        }

        //Validate skillset when selected already 
        private List<SelectListItem> AvailSkillSet()
        {
            int id = Convert.ToInt32(HttpContext.Session.GetString("ID"));

            List<SkillSets> allSkillSets = skillContext.GetAllSkill();

            List<StudentSkillSet> studentSkillSets = studentContext.GetStudentSkills(id);

            List<SelectListItem> AvailSkillSet = new List<SelectListItem>();

            foreach (SkillSets s in allSkillSets)
            {
                bool found = false;

                for (int i = 0; i < studentSkillSets.Count; i++)
                {

                    int ss = studentSkillSets[i].SkillSetID;

                    if (s.SkillSetID == ss)
                    {

                        found = true;
                        break;
                    }


                }

                if (found == false)
                {


                    AvailSkillSet.Add(
                        new SelectListItem
                        {
                            Value = s.SkillSetID.ToString(),
                            Text = s.SkillSetName
                        });
                    TempData["NoSkills"] = "";
                }

               else if(found == true)
                {

                    
                    TempData["NoSkills"] = "No Skillset found !";
                }

                
         
               
            }
            return AvailSkillSet;
        }




        public StudentUpdate MaptoStudentVM(StudentUpdate student)
        {

            student = studentContext.GetStudentUpdate(Convert.ToInt32(HttpContext.Session.GetString("ID")));
            StudentUpdate studentVM = new StudentUpdate()
            {

                StudentID = student.StudentID,
                Name = student.Name,
                Photo = student.Photo + ".jpg",
                Description = student.Description


            };
            return studentVM;
        }

        [HttpGet]
        public ActionResult UploadPhoto()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            StudentUpdate student = studentContext.GetStudentUpdate(Convert.ToInt32(HttpContext.Session.GetString("ID")));
            StudentUpdate studentVM = MaptoStudentVM(student);

            return View(studentVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadPhoto(StudentUpdate studentVM)
        {
            if (studentVM.FileToUpload != null &&
                studentVM.FileToUpload.Length > 0)
            {

                try
                {

                    // Find the filename extension of the file to be uploaded.       
                    string fileExt = Path.GetExtension(
                        studentVM.FileToUpload.FileName);
                    // Rename the uploaded file with the staff’s name.     
                    string uploadedFile = studentVM.Name + fileExt;
                    // Get the complete path to the images folder in server      
                    string savePath = Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot\\images\\Student", uploadedFile);

                    // Upload the file to server              
                    if (fileExt == ".jpg")
                    {
                        using (var fileSteam = new FileStream(savePath, FileMode.Create))
                        {
                            await studentVM.FileToUpload.CopyToAsync(fileSteam);
                        }
                        studentVM.Photo = uploadedFile;
                        studentContext.Upload(uploadedFile, studentVM.StudentID);
                        ViewData["Message"] = "File uploaded successfully.";
                    }
                    else
                    {
                        ViewData["Message"] = "File uploading fail!";
                    }

                }

             
                catch (Exception ex) //Other type of error       
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return View(studentVM);
        }

        //View Suggestion details
        public IActionResult ViewMentor()
        {
            int id = Convert.ToInt32(HttpContext.Session.GetString("ID"));
            List<Suggestion> SuggestionList = studentContext.GetSuggestions(id);
            return View(SuggestionList);
        }
    
        //Method for Acknowledge 
        public ActionResult Acknowledge(int suggestionid)
        {
            if (ModelState.IsValid)
            {

                studentContext.Acknowledge(suggestionid);

                
            }
            //Input validation fails, return to the view    
            //to display error message            
            return RedirectToAction("ViewMentor", "Student");
        }
    }
}