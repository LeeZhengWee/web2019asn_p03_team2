﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Protocols;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication1.Controllers
{
    public class SkillSetController : Controller
    {
        private SkillDAL skillContext = new SkillDAL();
        private StudentDAL studentContext = new StudentDAL();


        // GET: SkillSet
        public ActionResult Index(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["ViewStudents"] = "";
            SkillSetsViewModel skillVM = new SkillSetsViewModel();
            skillVM.SkillLists = skillContext.GetAllSkill();
            skillVM.StudentLists = new List<Student>();
            // BranchNo (id) present in the query string

            if (id != null)
            {
                ViewData["ViewStudents"] = "Full";
            }
            else
            {
                return View(skillVM);
            }

            List<Student> students = skillContext.GetAllStudentSkillSet(id.Value);
            if (students == null)
            {
                ViewData["ViewStudents"] = "Empty";
                return View(skillVM);
            }

            skillVM.StudentLists = students;
            return View(skillVM);
        }


        // GET: Staff/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            SkillSets skillsets = skillContext.GetDetails(id.Value);

            if (skillsets == null)
            {
                //Return to listing page, not allowed to edit                 
                return RedirectToAction("Index");
            }

            return View(skillsets);
        }

        // POST: Staff/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SkillSets skillSets)
        {
            //Get branch list for drop-down list              
            //in case of the need to return to Edit.cshtml view             
            if (ModelState.IsValid)
            {
                //Update staff record to database                 
                skillContext.Update(skillSets);
                return RedirectToAction("Index");
            }

            //Input validation fails, return to the view to display error message             
            return View(skillSets);
        }

        public ActionResult Create()
        {
            //// Stop accessing the action if not logged in or account not in the "Staff" role             
            //if ((HttpContext.Session.GetString("Role") == null) ||
            //    (HttpContext.Session.GetString("Role") != "Staff"))
            //{
            //    return RedirectToAction("Index", "Home");
            //}
            return View();
        }

        // POST: Staff/Create     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SkillSets skillSets)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (ModelState.IsValid)
            {
                //Add staff record to database      
                skillSets.SkillSetID = skillContext.Add(skillSets);
                //Redirect user to Staff/Index view               
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view to display error message                
                return View(skillSets);
            }
        }
        public ActionResult Delete(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                       (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit     
                return RedirectToAction("Index");
            }
            SkillSets skillset = skillContext.GetDetails(id.Value);
            if (skillset == null)
            {
                //Return to listing page, not allowed to edit       
                return RedirectToAction("Index");
            }
            return View(skillset);
        }


        //POST: Skill/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(SkillSets skillSets)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            //Delete skill record from DATABASE
            skillContext.Delete(skillSets.SkillSetID);
            return RedirectToAction("Index");
        }

     
    }
}

