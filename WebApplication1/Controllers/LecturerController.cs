﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.DAL;
using Microsoft.AspNetCore.Http;

namespace WebApplication1.Controllers
{
    public class LecturerController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();


        public IActionResult Index()
        {
            return View();
        }

        public ActionResult CreateProfile()
        {

            Lecturer lecturer = new Lecturer { Password = "p@55Mentor" };
            return View(lecturer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProfile(Lecturer lecturer)
        {
            if (ModelState.IsValid)
            {
                lecturer.Name = lecturerContext.AddLecturer(lecturer);

                return RedirectToAction("LecturerLogin", "Home");

            }

            else
            {
                return View(lecturer);
            }
        }


        [HttpGet]
        public ActionResult PostSuggestion()
        { 


            return View();
        }

        public ActionResult PostSuggestion(Suggestion suggestion)
        {
            if (ModelState.IsValid)
            {
                suggestion.SuggestionID = lecturerContext.Add(suggestion);
                return RedirectToAction("PostSuggestion");

            }

            else
            {
                return View(suggestion);
            }
        }

        public ActionResult ViewSuggestion()
        {
            List<Suggestion> suggestionList = lecturerContext.GetSuggestions();
            return View(suggestionList);
        }

        public ActionResult ViewMentees()
        {
            List<Student> studentList = lecturerContext.GetAllStudents();
            return View(studentList);
        }

        public ActionResult UpdatePassword()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                TempData["Message"] = "You must be logged in to access this page.";
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult UpdatePassword(IFormCollection formData)
        {
            string oldPass = formData["oldPass"].ToString();
            string newPass = formData["newPass"].ToString();
            string confirmPass = formData["confirmPass"].ToString();
            string lecturerID = HttpContext.Session.GetString("ID");

            bool verifiedOldPass = lecturerContext.accountChecker(lecturerID, oldPass);

            if (verifiedOldPass)
            {
                if (newPass == confirmPass)
                {
                    int count = newPass.Length;

                    if (count >= 8)
                    {
                        lecturerContext.UpdatePassword(lecturerID, newPass);
                        TempData["Message1"] = "You have sucessfully changed your password.";
                        
                    }
                    else {
                        TempData["Message1"] = "Password should be more than 8 characters.";
                    }
                }
                else
                {
                    TempData["Message1"] = "Password do not match.";
                }
            }
            else
            {
                TempData["Message1"] = "Old password is incorrect.";
            }
            return View();

        }

    }
}