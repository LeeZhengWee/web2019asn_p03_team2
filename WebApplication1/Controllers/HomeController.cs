﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebApplication1.DAL;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();
        private StudentDAL studentContext = new StudentDAL();



        public IActionResult Index()
        {

            return View();
        }
        public ActionResult StudentLogin()
        {
           
            return View();
        }

        public ActionResult LecturerLogin()
        {
            return View();
        }



        [HttpPost]
        public ActionResult StudentLogin(IFormCollection formData)
        {
            string login = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();

            Student student = studentContext.VerifyStudentLogin(login, password);

            if (student.Name != null || student.Password != null)
            {
                HttpContext.Session.SetString("Role", "Student");
                HttpContext.Session.SetString("Name", student.Name);
                HttpContext.Session.SetString("ID", student.StudentID.ToString());
                HttpContext.Session.SetString("logintime", DateTime.Now.ToString());
                return RedirectToAction("Index", "Student");
            }
            else
            {
                TempData["Message"] = "Invalid Login Credentials";
                return RedirectToAction("StudentLogin","Home");
            }

        }

        [HttpPost]
        public ActionResult LecturerLogin(IFormCollection formData)
        {
            // Read inputs from textboxes 
            // Email address converted to lowercase 
            string login = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();

            //if (loginID == "Peter_Ghim@ap.edu.sg" && password == "p@55Mentor")
            //{


            //    HttpContext.Session.SetString("Role", "Lecturer");
            //    // Redirect user to the "Lecturer_Home" 
            //    return RedirectToAction("LecturerHome");

            //}

            //else
            //{
            //    //Redirect user back to Lecturer Login page.
            //    return RedirectToAction("LecturerLogin");
            //}

            Lecturer lecturer = lecturerContext.VerifyLecturerLogin(login, password);

            if (lecturer != null )
            {
                HttpContext.Session.SetString("Role", "Lecturer");
                HttpContext.Session.SetString("Name", lecturer.Name);
                HttpContext.Session.SetString("ID", lecturer.LecturerID.ToString());
                HttpContext.Session.SetString("logintime", DateTime.Now.ToString());

                return RedirectToAction("LecturerHome");
            }
            else
            {
                TempData["Message"] = "Invalid Login Credentials";
                return RedirectToAction("LecturerLogin", "Home");
            }
        }


        public ActionResult LogOut()
        {
            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller
            return RedirectToAction("StudentLogin", "Home");
        }

        public ActionResult LecturerHome()
        {
            // Stop accessing the action if not logged in             
            // or account not in the "Staff" role             
            //if ((HttpContext.Session.GetString("Role") == null) ||
            //    (HttpContext.Session.GetString("Role") != "Lecturer"))
            //{
            //    return RedirectToAction("LecturerLogin", "Home");
            //}

            return View();
        }

        ////Create Student
        //public IActionResult StudentCreate()
        //{

        //    Student student = new Student { Password = "p@55Student" };
        //    return View(student);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult StudentCreate(Student student)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        student.StudentID = studentContext.Add(student);

        //        return View("StudentLogin");
        //    }

        //    else
        //        return View(student);
        //}



    }

}