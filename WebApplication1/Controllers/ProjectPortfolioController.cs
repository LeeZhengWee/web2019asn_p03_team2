﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.DAL;
using WebApplication1.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;

namespace WebApplication1.Controllers
{
    public class ProjectPortfolioController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }
        private ProjectDAL projectContext = new ProjectDAL();
        private StudentDAL studentContext = new StudentDAL();
        private LecturerDAL lecturerContext = new LecturerDAL();

        List<Project> myprojectList = new List<Project>();
        List<ProjectMember> projectmemberList = new List<ProjectMember>();

        public ActionResult ViewProject()
        {

            // GET: Project
            string user = HttpContext.Session.GetString("ID");
            List<ProjectMember> templist = projectContext.GetAllProjectMembers();
            List<Project> projectList = projectContext.GetAllProjects();
            foreach (ProjectMember m in templist)
            {
                if (m.StudentID.ToString() == user)
                {
                    projectmemberList.Add(m);
                }

            }

            foreach (Project p in projectList)
            {
                foreach (ProjectMember m in projectmemberList)
                {
                    if (m.ProjectID == p.ProjectId)
                    {
                        myprojectList.Add(p);
                    }
                }
            }
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }

            return View(myprojectList);



        }
        public ActionResult CreateProject()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProject(Project project)
        {
            string user = HttpContext.Session.GetString("ID");
            int studentId;
            List<ProjectMember> pmlist = projectContext.GetAllProjectMembers();
            
            if (ModelState.IsValid)
            {
                //Add Project record to database
                project.ProjectId = projectContext.Add(project);
                foreach(ProjectMember projectMember in pmlist)
                {
                    if(projectMember.StudentID.ToString() == user)
                    {
                        studentId = projectMember.StudentID;
                        projectMember.ProjectID = project.ProjectId;
                        projectMember.StudentID = projectContext.Addpm(projectMember);
                        
                        break;
                        
                    }
                }
                
            
                //Redirect user to Project/Index view
                return RedirectToAction("ViewProject");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(project);
            }
        }
        public ActionResult Details(int id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }
            Project project = projectContext.GetDetails(id);
            ProjectViewModel projectVM = MapToProjectVM(project);
            return View(projectVM);
        }
        public ProjectViewModel MapToProjectVM(Project project)
        {   
            ProjectViewModel projectVM = new ProjectViewModel
            {
                ProjectId = project.ProjectId,
                ProjectTitle = project.Title,
                Description = project.Description,
                ProjectPoster = project.ProjectPoster,
                ProjectURL = project.ProjectURL

            };
            return projectVM;
        }
        public ActionResult UploadPhoto(int id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Student" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Student"))
            {
                return RedirectToAction("Index", "Home");
            }
            Project project = projectContext.GetDetails(id);
            ProjectViewModel projectVM = MapToProjectVM(project);
            return View(projectVM);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadPhoto(ProjectViewModel projectVM,Project project)
        {
            if (projectVM.FileToUpload != null &&
            projectVM.FileToUpload.Length > 0)
            {
                try
                {
                    // Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(
                     projectVM.FileToUpload.FileName);
                    // Rename the uploaded file with the Project’s name.
                    string uploadedFile = projectVM.ProjectTitle + fileExt;
                    // Get the complete path to the images folder in server
                    string savePath = Path.Combine(
                     Directory.GetCurrentDirectory(),
                     "wwwroot\\images", uploadedFile);
                    // Upload the file to server
                    using (var fileSteam = new FileStream(
                     savePath, FileMode.Create))
                    {
                        await projectVM.FileToUpload.CopyToAsync(fileSteam);
                    }
                    projectVM.ProjectPoster = uploadedFile;
                    project.ProjectId = projectVM.ProjectId;
                    project.ProjectPoster = uploadedFile;
                    projectContext.changephoto(project);
                    ViewData["Message"] = "File uploaded successfully.";
                }
                catch (IOException)
                {
                    //File IO error, could be due to access rights denied
                    ViewData["Message"] = "File uploading fail!";
                }
                catch (Exception ex) //Other type of error
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return View(projectVM);
        
    }
      

        public ActionResult Edit(int? id)
        {
            List<Student> studentList = lecturerContext.GetAllStudents();
            List<Student> addstudentList = new List<Student>();
            List<SelectListItem> addList = new List<SelectListItem>();
            List<ProjectViewModel> studentinside = new List<ProjectViewModel>();
            List<ProjectMember> projectMemberslist = projectContext.GetAllProjectMembers();
            
            foreach (Student s in studentList)
            {

                addstudentList.Add(s);

            }

            foreach (Student i in addstudentList)
            {
                addList.Add(new SelectListItem
                {
                    Value = i.StudentID.ToString(),
                    Text = i.StudentID.ToString(),
                });


            }

            ViewData["StudentList"] = addList;
            if (id == null) //Query string parameter not provided
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

           
            ProjectViewModel project = projectContext.GotDetails(id.Value);
            if (project == null)
            {
                return RedirectToAction("ViewProject");
            }
            return View(project);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectViewModel project)
        {
            List<ProjectViewModel> port = new List<ProjectViewModel>();

            if (project.ProjectURL == null)
            {
                project.ProjectURL = "";
            }
            //Get branch list for drop-down list
            //in case of the need to return to Edit.cshtml view
            if (ModelState.IsValid)
            {
                //Update Project record to database
                projectContext.Update(project);
                projectContext.Addmember(project);
                return RedirectToAction("ViewProject");
            }
            //Input validation fails, return to the view
            //to display error message
            return View(project);
        }
            public ActionResult ProjectDetails()
        {
            return View();
        }
        public ActionResult CreateResult()
        {
            return View();
        }

    }
}