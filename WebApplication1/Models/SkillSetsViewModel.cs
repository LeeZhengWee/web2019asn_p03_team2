﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SkillSetsViewModel
    {
        public List<SkillSets> SkillLists { get; set; }
        public List<Student> StudentLists { get; set; }

    }
    
}
