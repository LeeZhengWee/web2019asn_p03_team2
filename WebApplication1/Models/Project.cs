﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Project
    {
        public int ProjectId { get; set; }
        [MaxLength(255)]
        public string Title { get; set; }
        [MaxLength(3000)]
        public string Description { get; set; }
        [MaxLength(255)]
        public string ProjectPoster { get; set; }
        [MaxLength(255)]
        public string ProjectURL { get; set; }

        
    }
}
