﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebApplication1.Models
{
    public class StudentUpdate
    {

        [Display(Name = "Student ID")]
        public int StudentID { get; set; }

        [Display(Name = "Student Name")]
        public string Name { get; set; }

        [Display(Name = "Photo")]
        [StringLength(255, ErrorMessage = "cannot exceed 255 characters")]
        public string Photo { get; set; }

        [Display(Name = "Description")]
        [StringLength(3000, ErrorMessage = "cannot exceed 3000 characters")]
        public string Description { get; set; }

        [Display(Name = "Achievement")]
        [StringLength(3000, ErrorMessage = "cannot exceed 3000 characters")]
        public string Achievement { get; set; }

        [Display(Name = "SkillSets")]
        public int SkillSetID { get; set; }

        public IFormFile FileToUpload { get; set; }


    }
}
