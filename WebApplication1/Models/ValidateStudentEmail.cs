﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApplication1.DAL;

namespace WebApplication1.Models
{
    public class ValidateStudentEmail : ValidationAttribute
    {
        private StudentDAL studentContext = new StudentDAL();

        public override bool IsValid(object value)
        {
            string email = Convert.ToString(value);
            if (studentContext.IsEmailExist(email))
                return false;
            // validation failed      
            else
                return true;
            // validation passed             
        }
    }
}
