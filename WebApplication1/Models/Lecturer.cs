﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Lecturer
    {
        [Display(Name = "LecturerID")]
        public int LecturerID { get; set; }

        [Display(Name = "Name")]
        [StringLength(50, ErrorMessage = "Name cannot exceed the 50 character limit.")]
        public string Name { get; set; }

        [Display(Name = "Email Address")]
        [StringLength(50, ErrorMessage ="Email Address cannot exceed the 50 chracter limit.")]
        public string EmailAddr { get; set; }

        [Display(Name = "Password")]
        [StringLength(255, ErrorMessage = "Password cannot exceed 255 character limit.")]
        public string Password { get; set; }

        [Display(Name = "Description")]
        [StringLength(3000, ErrorMessage = "Description cannot exceed 3000 character limit.")]
        public string Description { get; set; }


    }
}
