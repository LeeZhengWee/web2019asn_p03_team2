﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Suggestion
    {
        [Display(Name = "SuggestionID")]
        [Required(ErrorMessage = "Suggestion ID is required")]
        public int SuggestionID { get; set; }

        //Lecturer Name
        public string LecturerName { get; set; }

        [Display (Name = "LecturerID")]
        [Required(ErrorMessage = "Lecturer ID is required")]
        public int LecturerID { get; set; }

        [Display (Name = "StudentID")]
        [Required(ErrorMessage = "Student ID is required")]
        public int StudentID { get; set; }

        [Display (Name = "Description")]
        [StringLength (3000, ErrorMessage = "Cannot exceed 3000 characters.")]
        public string Description { get; set; }

        [Display (Name = "Status")]
        [Required(ErrorMessage = "Status is required")]
        public char Status { get; set; }

        [Display (Name = "DateCreated")]
        [Required(ErrorMessage = "DateCreated is required")]
        public DateTime DateCreated { get; set; }
    }
}
