﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SuggestionViewModel
    {
        public List<Suggestion> viewSuggestionList { get; set; }
        public List<string> viewNameList { get; set; }
    }
}
