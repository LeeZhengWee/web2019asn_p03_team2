﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SkillSets
    {
        [Display(Name = "SkillSetID")]
        public int SkillSetID { get; set; }

        [Display(Name = "SkillSetName")]
        // Custom Validation Attribute for checking email address exists 
        [ValidateSkillExists(ErrorMessage = "Skill Already Exists!")]
        public string SkillSetName { get; set; }

    }
}
