﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebApplication1.Models
{
    public class ProjectViewModel
    {
        public int ProjectId { get; set; }
        public int StudentID { get; set; }
        [MaxLength(255)]
        public string ProjectTitle { get; set; }
        [MaxLength(3000)]
        public string Description { get; set; }
        [MaxLength(255)]
        public string ProjectPoster { get; set; }
        [MaxLength(255)]
        public string ProjectURL { get; set; }
        public string Title { get; set;}
        public IFormFile FileToUpload { get; set; }
    }
}
