﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApplication1.DAL;

namespace WebApplication1.Models
{
    public class ValidateSkillExists : ValidationAttribute
    {
        private SkillDAL skillContext = new SkillDAL();

        public override bool IsValid(object value)
        {
            string skillname = Convert.ToString(value);
            if (skillContext.IsSkillNameExist(skillname))
                return false; // validation failed
            else
                return true; // validation passed     
        }   
    }
}
