﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebApplication1.Models
{
    public class StudentViewModel
    {

        [Display(Name = "ID")]
        public int StudentID { get; set; }

        [Display(Name = "Student Name")]
        public string Name { get; set; }

        [Display(Name = "Student Description")]
        public string Description { get; set; }

        [Display(Name = "Student Achievement")]
        public string StudentAchievement { get; set; }

        [Display(Name = "Student Picture")]
        public string Photo { get; set; }

        [Display(Name = "Student link ")]
        public string ExternalLink { get; set; }

        [Display(Name = "Student EmailAddress")]
        public string EmailAddr { get; set; }

        [Display(Name = "Student Course")]
        public string Course { get; set; }

        [Display(Name = "Suggestion")]
        public List<Suggestion> Suggestions { get; set; }


    }
}
