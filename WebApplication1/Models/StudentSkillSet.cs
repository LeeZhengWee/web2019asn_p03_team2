﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class StudentSkillSet
    {
        [Display(Name = "Student ID")]
        public int StudentID { get; set; }
       
        public int SkillSetID { get; set; }
    }
}
