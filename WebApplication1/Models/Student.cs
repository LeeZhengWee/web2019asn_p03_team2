﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Student
    {
        [Required]
        [Display(Name = "Student ID")]
        public int StudentID { get; set; }

        [Required]
        [Display(Name = "Name")]
        [StringLength(50, ErrorMessage = "cannot exceed 50 characters")]
        public string Name { get; set; }

        [Display(Name = "Course")]
        [StringLength(50, ErrorMessage = "cannot exceed 50 characters")]
        public string Course { get; set; }

        [Display(Name = "Photo")]
        [StringLength(255, ErrorMessage = "cannot exceed 255 characters")]
        public string Photo { get; set; }

        [Display(Name = "Description")]
        [StringLength(3000, ErrorMessage = "cannot exceed 3000 characters")]
        public string Description { get; set; }

        [Display(Name = "Achievement")]
        [StringLength(3000, ErrorMessage = "cannot exceed 3000 characters")]
        public string Achievement { get; set; }

        [Display(Name = "Link")]
        [StringLength(255, ErrorMessage = "cannot exceed 255 characters")]
        public string ExternalLink { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        [ValidateStudentEmail(ErrorMessage = "Email address already exists!")]
        [StringLength(50, ErrorMessage = "cannot exceed 50 characters")]
        public string EmailAddr { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "cannot exceed 255 characters")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Mentor ID")]
        public int MentorID { get; set; }

    }
}
