﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class ProjectMember
    {
        public int ProjectID { get; set; }
        public int  StudentID { get; set; }
        [MaxLength(50)]
        public string Role { get; set; }
    }
}
